# Quick Podman on Mac

## Installation and VM Config

I did start with the basic instructions [here](https://podman.io/getting-started/installation), with one modification:

* The default `podman machine init` creates a VM too small for most of our needs. To enable at least a `GitLab` container plus `congregate` use:

```
podman machine init --disk-size 50 --cpus 8 --memory 8192
```

## SSH Keys to the VM

* `podman machine init` will create default keys, etc, to allow ssh into the VM with `podman machine ssh`. If you wish to change the key:
  * Create the new key using ssh-keygen
  ```
  ssh-keygen -t ed25519 -f ~/.ssh/podman
  ```
  * SSH into the existing VM
  ```
  podman machine ssh
  ```
  * You should be connected as the core user. Edit the authorized keys in the VM
  ```
  vi ~/.ssh/authorized_keys.d/ignition
  ```
  * Add the `podman.pub` key to the end of the file and save
  * Back in your host box (ie: macbook) create a new connection and make it default
  ```
  podman system connection add --default --identity ~/.ssh/podman testssh ssh://core@localhost:49808/run/user/1000/podman/podman.sock
  ```
  * Test the connection
  ```
  podman info
  ```

## Podman Compose

[Podman Compose](https://github.com/containers/podman-compose) is a replacement for `docker compose` in Podman.

At this time, it has some problems with `volumes` in general, and `docker.sock` style bindings, as well

## Docker Socket

```bash
The system helper service is not installed; the default Docker API socket
address can't be used by podman. If you would like to install it run the
following commands:

	sudo /usr/local/Cellar/podman/4.1.1/bin/podman-mac-helper install
	podman machine stop; podman machine start

You can still connect Docker API clients by setting DOCKER_HOST using the
following command in your terminal session:

	export DOCKER_HOST='unix:///Users/gmiller/.local/share/containers/podman/machine/podman-machine-default/podman.sock'
```
